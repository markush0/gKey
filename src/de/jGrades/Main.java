package de.jGrades;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    public static final String LANGBUNDLE = "language";
    public static final String FXMLRESOURCE = "gkey.fxml";
    public static final String TITLE = "title";
    public static final int WIDTH = 1200;
    public static final int HEIGHT = 800;
    private double xOffset = 0;
    private double yOffset = 0;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {


        //Systemsprache holen

        Locale loc = Locale.getDefault();

        loadView(loc, primaryStage);
    }


    private void loadView(Locale locale, Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        ResourceBundle bundle = ResourceBundle.getBundle(LANGBUNDLE, locale);

        fxmlLoader.setResources(bundle);

//        Parent root = (Parent) fxmlLoader.load(this.getClass().getResource(FXMLRESOURCE).openStream());
        fxmlLoader.setLocation(getClass().getResource(FXMLRESOURCE));
        Parent root = fxmlLoader.load();


//
//        root.setOnMousePressed(new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                xOffset = event.getSceneX();
//                yOffset = event.getSceneY();
//            }
//        });
//        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                primaryStage.setX(event.getScreenX() - xOffset);
//                primaryStage.setY(event.getScreenY() - yOffset);
//            }
//        });


        primaryStage.setTitle(bundle.getString(TITLE));

        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT));

//        primaryStage.initStyle(StageStyle.UNDECORATED);


        primaryStage.show();
    }
}
