package de.jGrades;

public class Limit {
    private int top;
    private int bot;

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getBot() {
        return bot;
    }

    public void setBot(int bot) {
        this.bot = bot;
    }

    public Limit(int top, int bot) {
        this.top = top;
        this.bot = bot;
    }
}
