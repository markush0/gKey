package de.jGrades;

public class Grade {
    private double gradeValue;
    private double maxPoints;
    private double percTop;
    private double percBot;
    private double valTop;
    private double valBot;

    public double getGradeValue() {
        return gradeValue;
    }

    public double getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(double maxPoints) {
        this.maxPoints = maxPoints;
    }


    public Grade(double gradeValue, double percTop, double percBot, double mp) {
        this.gradeValue = gradeValue;
        this.percTop = percTop;
        this.percBot = percBot;
        this.maxPoints = mp;
    }

    public Grade(double gradeValue, double mp) {
        this.gradeValue = gradeValue;
        this.maxPoints = mp;
    }

    public double getPercTop() {
        return percTop;
    }

    public void setPercTop(double percTop) {
        this.percTop = percTop;
    }

    public double getPercBot() {
        return percBot;
    }

    public void setPercBot(double percBot) {
        this.percBot = percBot;
    }

    public double getValTop() {
        return valTop;
    }

    public void setValTop(double valTop) {
        this.valTop = valTop;
    }

    public double getValBot() {
        return valBot;
    }

    public void setValBot(double valBot) {
        this.valBot = valBot;
    }

    public void calc() {
        //TODO: Calculate values
        this.valTop = this.maxPoints * (this.percTop / 100);
        this.valBot = this.maxPoints * (this.percBot / 100);

        this.valBot = Math.rint(this.valBot);
        this.valTop = Math.rint(this.valTop);
    }

    @Override
    public String toString() {
        return "Grade{" +
                "gradeValue=" + gradeValue +
                ", percTop=" + percTop +
                ", percBot=" + percBot +
                ", valTop=" + valTop +
                ", valBot=" + valBot +
                '}';
    }
}
