package de.jGrades;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Key {
    Logger log;
    private HashMap<Integer, Grade> map;
    private HashMap<Integer, Limit> mapLimits;
    private final int maxPoints = 20;

    public Key() {
        this.log = Logger.getLogger(getClass().getName());

        this.map = new HashMap<Integer, Grade>();
        this.mapLimits = new HashMap<Integer, Limit>();


        //Notenwerte anlegen
        this.addToMap(1);
        this.addToMap(2);
        this.addToMap(3);
        this.addToMap(4);
        this.addToMap(5);
        this.addToMap(6);

        //Prozentgrenzen anlegen
        this.addToLimitMap(1, 100, 93);
        this.addToLimitMap(2, 92, 80);
        this.addToLimitMap(3, 79, 65);
        this.addToLimitMap(4, 64, 45);
        this.addToLimitMap(5, 44, 22);
        this.addToLimitMap(6, 21, 0);



        this.map.forEach((k, v) -> v.setPercBot(this.mapLimits.get(k).getBot()));
        this.map.forEach((k, v) -> v.setPercTop(this.mapLimits.get(k).getTop()));
        this.map.forEach((k,v) -> v.calc());
        this.map.forEach((k,v) -> log.log(Level.INFO, v.toString()));


    }

    public Key(HashMap<Integer, Grade> map) {
        this.map = map;
    }

    private void addToMap(int grade) {
        Grade g = new Grade(grade, this.maxPoints);
        this.map.put(grade, g);
    }

    private void addToLimitMap(int grade, int top, int bot) {
        Limit lim = new Limit(top, bot);

        this.mapLimits.put(grade, lim);
    }
}